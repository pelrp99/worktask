import React from "react";
import logo from "../images/logo.png";

const Navbar = () => {
  return (
    <>
      <nav className="navbar">
        <img src={logo} alt="" width="50px" />
        <div>q</div>
      </nav>
    </>
  );
};

export default Navbar;
