import axios from "axios";
import React, { useEffect, useState } from "react";
axios.defaults.baseURL = "https://dummyjson.com";
const useAxios = (url) => {
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    return async () => {
      try {
        const res = await axios.get(url);
        setData(res.data);
      } catch (err) {
        setError(err.message);
      } finally {
        setLoading(false);
      }
    };
  }, [url]);
  return { data, isLoading, error, setData };
};
export default useAxios;
