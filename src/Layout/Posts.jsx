import React from "react";
import edit from "../images/editing.png";
import remove from "../images/remove.png";
import add from "../images/plus.png";
import useAxios from "../Hooks/useAxios";
const Posts = () => {
  const { data, isLoading, error, setData } = useAxios("/users");
  // console.log(data.users);
  return (
    <div className="posts-body">
      <div className="posts-buttons">
        <button className="add-button">
          <img src={add} alt="" width="15px" />
        </button>
        <button className="edit-button">
          <img src={edit} alt="" width="15px" />
        </button>
        <button className="delete-button">
          <img src={remove} alt="" width="15px" />
        </button>
      </div>
      <div className="posts-name">
        {data?.users?.map((item) => (
          <button key={item}>{item.id}</button>
        ))}
      </div>
    </div>
  );
};

export default Posts;
