import React from "react";
import Navbar from "../Pages/navbar";
import Posts from "./Posts";
import Comments from "./Comments";

const RootLayout = () => {
  return (
    <header>
      <div className="container">
        <Navbar />
        <div className="">
          <Posts />
          <Comments />
        </div>
      </div>
    </header>
  );
};

export default RootLayout;
