import RootLayout from "./Layout/RootLayout";
import "../src/App.css";
function App() {
  return (
    <div>
      <RootLayout />
    </div>
  );
}
export default App;
